using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotRotateToDir : MonoBehaviour
{
    public Vector3 velocity;
    public float angle;
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 v = GetComponent<Rigidbody2D>().velocity;
        angle = Mathf.Atan2(v.x, -v.y) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
}
