using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TirProjectiles : MonoBehaviour
{
    public bool gameEnd = false;
    public Transform tirAInstancier;
    public List<GameObject> listeTireurs = new List<GameObject>();
    public GameObject SphereMilieu;
    public float cooldownDeTirs;
    float timerActuelDesTirs;
    public float speedTir;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!gameEnd && Time.time > timerActuelDesTirs + cooldownDeTirs)
        {
            Fire();
            timerActuelDesTirs = Time.time;
        }
    }
    void Fire()
    {
        GameObject tireurChoisi = listeTireurs[Random.Range(0,listeTireurs.Count)];
        //print(tireurChoisi.name);
        Transform tirinstancie;
        tirinstancie = Instantiate(tirAInstancier, new Vector3(tireurChoisi.transform.position.x, -2, tireurChoisi.transform.position.z), Quaternion.Euler(0, 0, 0)) as Transform;
        Rigidbody ebPrefab = tirinstancie.GetComponent<Rigidbody>();
        float xDif = SphereMilieu.transform.position.x - tireurChoisi.transform.position.x;
        float zDif = SphereMilieu.transform.position.z - tireurChoisi.transform.position.z;
        float totalDif = Mathf.Abs(xDif) + Mathf.Abs(zDif);
        xDif = xDif / totalDif;
        zDif = zDif / totalDif;
        //print("pioupiou");
        ebPrefab.AddForce(Vector3.forward * zDif * speedTir);
        ebPrefab.AddForce(Vector3.right * xDif * speedTir);

        Vector3 lookAtPos = SphereMilieu.transform.position - tirinstancie.transform.position;
        // lookAtPos.y = player.transform.position.y; // do not rotate the player around x
        Quaternion newRotation = Quaternion.LookRotation(lookAtPos, tirinstancie.transform.up);
        tirinstancie.transform.rotation = Quaternion.Slerp(tirinstancie.transform.rotation, newRotation, 1);
        AudioManager.instance.Play("Bulles qui arrivent");
    }
}
