using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class menu : MonoBehaviour
{
    public characStats characStats1;
    public characStats characStats2;
    public SpriteRenderer menuAccueil;
    public bool AccueilOpen = true;
    public SpriteRenderer menuFinWinJ1;
    public SpriteRenderer menuFinWinJ2;
    public SpriteRenderer menuFinNoWin;
    public bool EndScreenOpen = false;
    public GameObject bouleMilieu;
    public GameObject vraiBouleMilieu;
    public GameObject tirProje;

    public float timerAvantDePasserLaFin = 1;
    float timerAvantLaFin = 0;

    public float miniBulles = 2;
    public float maxiBulles = 4;
    float timerActuelBulles = 0;
    float prochainTimerBulles = 2;
    // Start is called before the first frame update
    void Start()
    {
        AudioManager.instance.Play("music");
        Time.timeScale = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time > timerActuelBulles + prochainTimerBulles)
        {
            AudioManager.instance.Play("Bulles");
            prochainTimerBulles = Random.Range(miniBulles, maxiBulles);
            timerActuelBulles = Time.time;
        }

        if(AccueilOpen && ( characStats1.player.GetAxisRaw(characStats1.dashButton) == 1 || characStats2.player.GetAxisRaw(characStats2.dashButton) == 1))
        {
            Time.timeScale = 1;
            AccueilOpen = false;
            menuAccueil.enabled = false;
        }
        else if (EndScreenOpen && Time.time > timerAvantDePasserLaFin+timerAvantLaFin && (characStats1.player.GetAxisRaw(characStats1.dashButton) == 1 || characStats2.player.GetAxisRaw(characStats2.dashButton) == 1))
        {
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }
    }
    public void displayWinScreen(string winner)
    {
        if (!EndScreenOpen)
        {
            characStats1.end = true;
            characStats2.end = true;
            // lancer anim de mort et de win ou l'anim de deux morts
            if (winner == "J1")
            {
                menuFinWinJ1.enabled = true;
                characStats1.animator.SetTrigger("Win");
                characStats2.animator.SetTrigger("Death");
                AudioManager.instance.Play("Win");
                AudioManager.instance.Play("Death");
                AudioManager.instance.Play("Cut");
            }
            else if (winner == "J2")
            {
                menuFinWinJ2.enabled = true;
                characStats1.animator.SetTrigger("Death");
                characStats2.animator.SetTrigger("Win");
                AudioManager.instance.Play("Win");
                AudioManager.instance.Play("Death");
                AudioManager.instance.Play("Cut");
            }
            else if (winner == "noWin")
            {
                menuFinNoWin.enabled = true;
                characStats1.animator.SetTrigger("Death");
                characStats2.animator.SetTrigger("Death");
                AudioManager.instance.Play("DualDeath");
                AudioManager.instance.Play("Death");
            }
            EndScreenOpen = true;
            timerAvantLaFin = Time.time;
            tirProje.GetComponent<TirProjectiles>().gameEnd = true;
            //bouleMilieu.SetActive(false);
            //vraiBouleMilieu.SetActive(true);
            //vraiBouleMilieu.transform.parent = null;

        }
    }
}
