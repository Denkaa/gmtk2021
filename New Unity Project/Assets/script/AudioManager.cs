﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    public AudioMixer audioMixer;
    [Header("Pause Music Parameters")]
    [Header("Music Cutoff")]
    public float playCutoffFreq;
    public float pauseCutoffFreq;
    [Header("Music Volume")]
    public float playVolume;
    public float pauseVolume;
    public float fadeDuration;

    [Header("New Game music crossfade")]
    public float crossFadeDuration;

    [Header("Sound List")]
    public Sound[] sounds;

    [HideInInspector] public bool appearingAnimationIsFinished;
    [HideInInspector] public bool disapearingLevelAnimationIsFinished;

    int lastIdForRandom = 5;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        //for using actual sfx with correct names
        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.multipleClips = new AudioClip[s.clipsNumber];
            if (!s.randomSounds && !s.followingSounds)
            {
                s.clip = Resources.Load<AudioClip>(s.path + s.name);
            }
            else if (s.randomSounds || s.followingSounds)
            {
                for (int i = 0; i < s.multipleClips.Length; i++)
                {
                    s.multipleClips[i] = Resources.Load<AudioClip>(s.path + s.name + " " + i);
                    if (s.multipleClips[i] == null)
                    {
                        Debug.LogWarning("Sound: " + s.path + s.name + " " + i + " not found");
                        return;
                    }
                }
            }
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.loop = s.loop;
            s.source.outputAudioMixerGroup = s.mixerGroup;
        }

        if (playCutoffFreq <= pauseCutoffFreq)
        {
            playCutoffFreq = 22000;
            pauseCutoffFreq = 550;
        }
        if (playVolume <= pauseVolume)
        {
            playVolume = 0;
            pauseVolume = -5;
        }
        
    }


    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found");
            return;
        }
        if (!s.randomSounds && !s.followingSounds)
        {

            //print("Audio/" + s.path + s.name);
            s.source.Play();
        }
        else if (s.randomSounds)
        {
            while (s.rndIndex == lastIdForRandom)
            {
                s.rndIndex = UnityEngine.Random.Range(0, s.multipleClips.Length);
            }
            s.source.clip = s.clip = s.multipleClips[s.rndIndex];
            lastIdForRandom = s.rndIndex;
            if (s.source.clip == null)
            {
                Debug.LogWarning("A sound must be missing in " + s.path + s.name);
                s.source.clip = s.clip = s.multipleClips[0];
            }
            s.source.Play();
        }
        else if (s.followingSounds)
        {
            s.source.clip = s.clip = s.multipleClips[s.currentIndex];
            if (s.source.clip == null)
            {
                Debug.LogWarning("A sound must be missing in " + s.path + s.name);
                s.source.clip = s.clip = s.multipleClips[0];
            }
            if (s.currentIndex == s.multipleClips.Length - 1)
            {
                Debug.Log(s.currentIndex);
                s.currentIndex = 0;
            }
            else
                s.currentIndex++;
            s.source.pitch = 1;
            s.source.Play();
        }
    }

    public void PlayUnique(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + s.path + name + " not found");
            return;
        }
        if (!s.randomSounds)
        {
            if (!s.source.isPlaying)
                s.source.Play();
        }
        else
        {
            s.rndIndex = UnityEngine.Random.Range(0, s.multipleClips.Length);
            s.source.clip = s.clip = s.multipleClips[s.rndIndex];
            if (s.source.clip == null)
            {
                Debug.LogWarning("A sound must be missing in " + s.path + s.name);
                s.source.clip = s.clip = s.multipleClips[0];
            }
            if (!s.source.isPlaying)
                s.source.Play();
        }
    }


    public void Stop(string name)
    {

        print("AUDIOSTOPI");
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            //Debug.LogWarning("Sound: " + s.path + name + " not found");
            return;
        }
        s.source.Stop();
    }

    public void MusicFadeOut(string fadeOutParamName, float FadeDuration, bool needToStop)
    {
        StartCoroutine(MusicFadeOutCorout(fadeOutParamName, FadeDuration,needToStop));
    }
    public void MusicFadeIn(string fadeInParamName, float FadeDuration)
    {
        StartCoroutine(MusicFadeInCorout(fadeInParamName, FadeDuration));
    }
    public void MusicSetVolumeToZero(string musicToSetToZero)
    {
        Sound s = Array.Find(sounds, sound => sound.name == musicToSetToZero);
        s.source.volume = 0;
    }
    public void invertPitch(string musicToinverse)
    {
        Sound s = Array.Find(sounds, sound => sound.name == musicToinverse);
        s.source.pitch = -1;
    }
    public void basicPitch(string musicToinverse)
    {
        Sound s = Array.Find(sounds, sound => sound.name == musicToinverse);
        s.source.pitch = 1;
    }
    public void MusicSetVolumeToOne(string musicToSetToOne)
    {
        Sound s = Array.Find(sounds, sound => sound.name == musicToSetToOne);
        s.source.volume = 1;
    }
    public void CrossFade(string musicToUp, string musicToDown, float FadeDuration)
    {
        StartCoroutine(MusicCrossFadeCorout(musicToUp,musicToDown, FadeDuration));
    }

    IEnumerator MusicCutoffCoroutine(float startMusicCutoff, float endMusicCutoff, float startMusicVolume, float endMusicVolume, float fadeDuration)
    {
        float currentTime = 0f;
        float normalizedValue;

        while (currentTime <= fadeDuration)
        {
            currentTime += Time.unscaledDeltaTime;
            normalizedValue = currentTime / fadeDuration;
            audioMixer.SetFloat("igMusicCutoff", Mathf.Lerp(startMusicCutoff, endMusicCutoff, normalizedValue));
            audioMixer.SetFloat("igMusicVolume", Mathf.Lerp(startMusicVolume, endMusicVolume, normalizedValue));
            yield return null;
        }
        audioMixer.SetFloat("igMusicCutoff", endMusicCutoff);
        audioMixer.SetFloat("igMusicVolume", endMusicVolume);
        yield return null;
    }

    public IEnumerator MusicCrossFade(AudioMixer audioMixer, string fadeOutParamName, string fadeInParamName, float crossFadeDuration, string fadedOutMusicName, string fadeInMusicName)
    {
        float currentTime = 0f;
        float normalizedValue;
        Play(fadeInMusicName);
        while (currentTime <= crossFadeDuration)
        {
            currentTime += Time.unscaledDeltaTime;
            normalizedValue = currentTime / crossFadeDuration;
            audioMixer.SetFloat(fadeOutParamName, Mathf.Lerp(0f, -80f, normalizedValue));
            audioMixer.SetFloat(fadeInParamName, Mathf.Lerp(-80f, 0f, normalizedValue));
            yield return null;
        }
        audioMixer.SetFloat(fadeOutParamName, -80f);
        audioMixer.SetFloat(fadeInParamName, 0f);
        Stop(fadedOutMusicName);
        yield return null;
    }

    public IEnumerator MusicFadeOutCorout(string fadeOutName, float FadeDuration,bool needToStop)
    {
        Sound s = Array.Find(sounds, sound => sound.name == fadeOutName);
        float volumeMax = s.volume;
        float currentTime = 0f;
        float normalizedValue;
        while (currentTime <= FadeDuration)
        {
            currentTime += Time.unscaledDeltaTime;
            normalizedValue = currentTime / FadeDuration;
            s.source.volume = 1-normalizedValue-(1-volumeMax);
            yield return null;
        }
        if (needToStop)
        {
            Stop(fadeOutName);
        }
        else
        {
            s.source.volume = 0;
        }
        yield return null;
    }
    public IEnumerator MusicFadeInCorout(string fadeInName, float FadeDuration)
    {
        Sound s = Array.Find(sounds, sound => sound.name == fadeInName);
        float volumeMax = s.volume;
        float currentTime = 0f;
        float normalizedValue;
        while (currentTime <= FadeDuration)
        {
            currentTime += Time.unscaledDeltaTime;
            normalizedValue = currentTime / FadeDuration;
            s.source.volume = 0 + normalizedValue - (1- volumeMax);
            yield return null;
        }
        s.source.volume = volumeMax;
        yield return null;
    }
    public IEnumerator MusicCrossFadeCorout(string fadeInName, string fadeOutName, float FadeDuration)
    {
        Sound sOut = Array.Find(sounds, sound => sound.name == fadeOutName);
        Sound sIn = Array.Find(sounds, sound => sound.name == fadeInName);
        float volumeMaxIn = sIn.volume;
        float volumeMaxOut = sOut.volume;
        float currentTime = 0f;
        float normalizedValue;
        while (currentTime <= FadeDuration)
        {
            //print("current time = " + currentTime);
            currentTime += Time.unscaledDeltaTime;
            normalizedValue = currentTime / FadeDuration;
            sIn.source.volume = 0 + normalizedValue - (1 - volumeMaxIn);
            sOut.source.volume = 1 - normalizedValue - (1 - volumeMaxOut);
            yield return null;
        }

        sIn.source.volume = volumeMaxIn;
        sOut.source.volume = 0;
        yield return null;
    }
    public void miseEnPause(bool miseEnPause)
    {
        print(audioMixer.FindSnapshot("normalGame")+"+"+ audioMixer.FindSnapshot("pauseGame"));
        if(miseEnPause)
        {
            audioMixer.TransitionToSnapshots(new AudioMixerSnapshot[] { audioMixer.FindSnapshot("normalGame"), audioMixer.FindSnapshot("pauseGame") },new float[] { 0.0f, 1.0f }, 0.0f);
        }
        else if (!miseEnPause)
        {
            audioMixer.TransitionToSnapshots(new AudioMixerSnapshot[] { audioMixer.FindSnapshot("pauseGame"), audioMixer.FindSnapshot("normalGame") }, new float[] { 0.0f, 1.0f }, 0.0f);
        }
    }
}
