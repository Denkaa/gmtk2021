using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class characStats : MonoBehaviour
{
    public GameObject enemy;
    public float distanceEntreJoueurs;
    public float speed;
    public string moveHorizontalButton = "Move Horizontal";[HideInInspector]
    public string moveVerticalButton = "Move Vertical";[HideInInspector]
    public string dashButton = "Dash";[HideInInspector]
    float lh;
    float lv;
    public Player player;
    Rigidbody playerRigidbody;
    private Vector3 movement;
    public int idDuJoueur;
    int frames = 0;
    public bool end = false;

    public GameObject lightDash;
    public bool canDash = false;
    public bool isDashing = false;
    public int speedWhileDashing = 30;
    public float timerSpeedUp = 0.2f;
    float timerPourLeSpeedUp;
    public float timerDash = 2f;
    float timerPourLeDash;
    public Animator animator;
    public GameObject popBulles;

    //stats pourle dash (cooldown,speed, distance...)

    void Start()
    {
        player = ReInput.players.GetPlayer(idDuJoueur);
        playerRigidbody = GetComponent<Rigidbody>();
        frames = idDuJoueur;
    }

    // Update is called once per frame
    void Update()
    {
        if (!end)
        {
            distanceEntreJoueurs = Vector3.Distance(enemy.transform.position, transform.position);
            //print(distanceEntreJoueurs);
            if (distanceEntreJoueurs > 8 && !isDashing)
            {
                speed = 5 - (distanceEntreJoueurs - 8);
                if (distanceEntreJoueurs > 10 && frames % 2 == 0)
                {
                    if (enemy.GetComponent<characStats>().isDashing)
                    {
                        transform.position += (enemy.transform.position - transform.position).normalized * distanceEntreJoueurs * 3 * Time.deltaTime;
                    }
                    else
                    {
                        transform.position += (enemy.transform.position - transform.position).normalized * distanceEntreJoueurs * 0.5f * Time.deltaTime;
                    }
                }
            }
            Vector3 direRota = enemy.transform.position - transform.position;
            Quaternion rota = Quaternion.LookRotation(direRota, Vector3.up);
            
            transform.rotation = rota;
            frames++;
            if (Time.time > timerPourLeSpeedUp + timerSpeedUp)
            {
                isDashing = false;
                speed = 5;
            }
            if (!canDash && Time.time > timerPourLeDash + timerDash)
            {
                lightDash.SetActive(true);
                canDash = true;
                if (idDuJoueur == 1)
                {
                    print("piouuuu1");
                    AudioManager.instance.Play("Recovera");
                }
                else
                {
                    print("piouuuu1Autere");
                    AudioManager.instance.Play("Recoverb");
                }
            }
        }
    }
    void FixedUpdate()
    {
        if (!end)
        {
            if (canDash && player.GetAxisRaw(dashButton) == 1)
            {
                //Instantiate(popBulles, transform);
                isDashing = true;
                canDash = false;
                speed = speedWhileDashing;
                timerPourLeSpeedUp = Time.time;
                timerPourLeDash = Time.time;
                print("miaoumiaou");
                animator.SetTrigger("Dash");
                enemy.GetComponent<characStats>().animator.SetTrigger("Grab");
                AudioManager.instance.Play("Dash");
                lightDash.SetActive(false);
            }
            lh = player.GetAxisRaw(moveHorizontalButton);
            lv = player.GetAxisRaw(moveVerticalButton);
            //print("lh = "+lh+"et lv ="+lv);
            if (lh != 0 || lv != 0)
            {
                Move(lh, lv);
                animator.SetBool("Run", true);
            }
            else
            {

                animator.SetBool("Run", false);
            }
        }
    }

    void Move(float lh,float lv)
    {

        movement.Set(lh, 0, lv);
        movement = movement.normalized * speed * Time.deltaTime;
        playerRigidbody.MovePosition(transform.position + movement);
    }

    //le truc qui augmente la taille de la corde, �a peut �tre la distance a peu pr�s entre les deux joueurs, quand elle atteint le max, c'est le joueur qui applique le plus de force qui fait ramenerl 'autre
}
