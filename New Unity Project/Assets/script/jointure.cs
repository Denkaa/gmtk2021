using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jointure : MonoBehaviour
{
    characStats characStats;
    public bool jointureQuiCasse = false;
    public int idJoueur;
    public GameObject menuDisplay;
    // Start is called before the first frame update
    void Start()
    {
        if (GameObject.Find("J1"))
        {
            characStats = GameObject.Find("J1").GetComponent<characStats>();
        }
        if (GameObject.Find("Menu"))
        {
            menuDisplay = GameObject.Find("Menu");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(jointureQuiCasse && characStats.distanceEntreJoueurs > 14)
        {
            menuDisplay.GetComponent<menu>().displayWinScreen("noWin");
            Destroy(this.gameObject);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        print("puouuu");
        if(other.tag == "Tir")
        {
            if(idJoueur == 0)
            {
                menuDisplay.GetComponent<menu>().displayWinScreen("J2");
            }
            else if (idJoueur == 1)
            {
                menuDisplay.GetComponent<menu>().displayWinScreen("J1");
            }
            Destroy(this.gameObject);
        }
    }
    //Au enter d'un projectile, appeler le destroy et voir le calcul de qui gagne
}
